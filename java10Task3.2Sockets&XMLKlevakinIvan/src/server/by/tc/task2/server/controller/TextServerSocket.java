package server.by.tc.task2.server.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TextServerSocket {

	private ServerSocket server;

	private static final Logger log = LogManager.getLogger(TextServerSocket.class);

	public void start() {
		try {
			server = new ServerSocket(4830);

			while (true) {

				log.info("Server starts!");
				Socket socket = server.accept();
				Thread newClient = new Thread(new ClientManager(socket));
				newClient.start();
			}
		} catch (IOException e) {
			log.error("Ops", e);
		} finally {
			try {
				if (server != null) {
					server.close();
				}
			} catch (IOException e) {
				log.error("Ops", e);
			}
		}
	}
}
