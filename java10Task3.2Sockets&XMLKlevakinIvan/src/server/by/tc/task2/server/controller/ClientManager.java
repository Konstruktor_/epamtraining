package server.by.tc.task2.server.controller;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import server.by.tc.task2.server.entity.Deposit;
import server.by.tc.task2.server.service.parser.dom.DOMDepositParser;
import server.by.tc.task2.server.service.ParserType;
import server.by.tc.task2.server.service.parser.sax.SAXDepositParser;
import server.by.tc.task2.server.service.parser.stax.StAXDepositParser;


import javax.xml.stream.XMLStreamException;
import java.io.*;
import java.net.Socket;
import java.util.ArrayList;


public class ClientManager implements Runnable {

	private static final Logger log = LogManager.getLogger(ClientManager.class);

	private static final int ANSWER_CLIENT_WAIT_TIME = 1000;

	private String path = System.getProperty("user.dir") + "\\src\\server\\resources\\bank.xml";
	private Socket socket;
	private InputStream in;
	private OutputStream out;
	private ObjectOutputStream oos;
	private ArrayList<Deposit> depositList;


	public ClientManager(Socket socket) {
		this.socket = socket;
	}

	@Override
	public void run() {

		try {

			in = socket.getInputStream();
			out = socket.getOutputStream();

			log.info("Read request from client");

			byte[] request =  readRequestFromClient();
			ArrayList<Deposit> response;

			while (request[0] != 0) {

				response = responseProcessing(request);
				sendResponseToClient(response);
				request =  readRequestFromClient();
			}
			
			sendCloseConfirmationToClient();

			log.info("Thread service of client exits.");
		} catch (IOException e) {
			log.error("Ops!", e);
		} catch (InterruptedException e) {
			log.error("Ops!", e);
		} catch (SAXException e) {
			log.error("Ops!", e);
		} catch (XMLStreamException e) {
			log.error("Ops!", e);
		}
	}

	private byte[] readRequestFromClient() throws InterruptedException,
			IOException {
		log.info("Read request from client");
		int i = in.available();
		while (i == 0) {
			Thread.sleep(ANSWER_CLIENT_WAIT_TIME);
			i = in.available();
		}
		byte[] b = new byte[i];
		in.read(b);
		return b;
	}
	
	private ArrayList<Deposit> responseProcessing(byte[] request) throws IOException, SAXException, XMLStreamException {
		log.info("Response processing.");
		String choice = new String(request);

		switch (choice) {
			case ParserType.DOM:

				DOMDepositParser domParser = new DOMDepositParser();
				log.info("DOM parsing started");
				depositList = domParser.parse(path);
				log.info("DOM parsing started");
				return depositList;

			case ParserType.SAX:

				SAXDepositParser saxParser = new SAXDepositParser();
				log.info("SAX parsing started");
				depositList = saxParser.parse(path);
				log.info("SAX parsing ended");
				return depositList;

			case ParserType.STAX:

				StAXDepositParser staxParser = new StAXDepositParser();
				log.info("StAX parsing started");
				depositList = staxParser.parse(path);
				log.info("StAX parsing ended");
				return depositList;

			default:
				return null;
		}
	}
	
	private void sendResponseToClient(ArrayList<Deposit> response) throws IOException{
		log.info("Send response to client.");
		oos = new ObjectOutputStream(out);
		oos.writeObject(response);
	}
	
	private void sendCloseConfirmationToClient() throws IOException{
		log.info("Send close confirmation to client.");
		out.write(new byte[]{0});
	}
}
