package server.by.tc.task2.server.start;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;
import server.by.tc.task2.server.controller.TextServerSocket;

import java.io.File;

public class Start {

	public static void main(String[] args) {

		LoggerContext context = (LoggerContext) LogManager.getContext(false);
		File file = new File(System.getProperty("user.dir")+ "\\src\\server\\log4j2.xml");
		context.setConfigLocation(file.toURI());

		TextServerSocket server = new TextServerSocket();

		server.start();
	}

}
