package server.by.tc.task2.server.service.parser.sax;


import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;
import server.by.tc.task2.server.entity.Constants;
import server.by.tc.task2.server.entity.Deposit;
import server.by.tc.task2.server.entity.DepositTagName;

import java.util.ArrayList;



public class DepositSAXHandler extends DefaultHandler {

    private ArrayList<Deposit> depositList = new ArrayList<Deposit>();
    private Deposit deposit;
    private StringBuilder text;

    public ArrayList<Deposit> getDepositList() {
        return depositList;
    }

    public void startElement(String uri, String localName, String qName,
                             Attributes attributes) throws SAXException {

        text = new StringBuilder();
        if (qName.equals(Constants.DEPOSIT)){
            deposit = new Deposit();
            deposit.setId(Integer.parseInt(attributes.getValue(Constants.ID)));
        }
    }

    public void characters(char[] buffer, int start, int length) {
        text.append(buffer, start, length);
    }


    public void endElement(String uri, String localName, String qName) throws SAXException {
        DepositTagName tagName = DepositTagName.valueOf(qName.toUpperCase().replace("-", "_"));
        switch (tagName) {
            case CURRENCY:
                deposit.setCurrency(text.toString());
                break;
            case TYPE:
                deposit.setType(text.toString());
                break;
            case DEPOSITOR:
                deposit.setDepositor(text.toString());
                break;
            case AMOUNT:
                deposit.setAmount(text.toString());
                break;
            case PROFITABILITY:
                deposit.setProfitability(text.toString());
                break;
            case TIME_CONSTRAINTS:
                deposit.setTimeConstraints(text.toString());
                break;
            case START_DATE_OF_DEPOSIT:
                deposit.setStartDateOfDeposit(text.toString());
                break;
            case DEPOSIT:
                depositList.add(deposit);
                deposit = null;
                break;
        }
    }
}
