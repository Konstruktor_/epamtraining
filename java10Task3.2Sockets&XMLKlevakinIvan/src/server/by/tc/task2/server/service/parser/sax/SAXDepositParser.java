package server.by.tc.task2.server.service.parser.sax;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;
import server.by.tc.task2.server.entity.Deposit;

import java.io.IOException;
import java.util.ArrayList;


public class SAXDepositParser {

    private XMLReader reader;
    private DepositSAXHandler handler;
    private ArrayList<Deposit> depositList;

    public ArrayList<Deposit> parse(String path) throws SAXException, IOException {

        reader =  XMLReaderFactory.createXMLReader();
        handler = new DepositSAXHandler();
        reader.setContentHandler(handler);
        reader.parse(new InputSource(path));
        depositList = handler.getDepositList();

        return depositList;
    }
}
