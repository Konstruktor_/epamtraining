package server.by.tc.task2.server.service.parser.dom;


import com.sun.org.apache.xerces.internal.parsers.DOMParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import server.by.tc.task2.server.entity.Constants;
import server.by.tc.task2.server.entity.Deposit;

import java.io.IOException;
import java.util.ArrayList;


public class DOMDepositParser {

    private DOMParser parser;
    private Document document;
    private NodeList depositNodes;
    private Element root;
    private Deposit deposit;

    private ArrayList<Deposit> depositList = new ArrayList<Deposit>();

    public ArrayList<Deposit> parse(String path) throws IOException, SAXException {

        parser = new DOMParser();
        parser.parse(path);
        document = parser.getDocument();
        root = document.getDocumentElement();
        depositNodes = root.getElementsByTagName(Constants.DEPOSIT);

        int length = depositNodes.getLength();

        for (int i = 0; i < length; i++) {

            deposit = new Deposit();
            fillDeposit(i);
            depositList.add(deposit);

        }
        return depositList;

    }

    private void fillDeposit(int i){
        Element depositElement = (Element) depositNodes.item(i);
        deposit.setId(Integer.parseInt(depositElement.getAttribute(Constants.ID)));

        deposit.setCurrency(getSingleChild(depositElement, Constants.CURRENCY).
                            getTextContent().trim());

        deposit.setType(getSingleChild(depositElement, Constants.TYPE).
                        getTextContent().trim());

        deposit.setDepositor(getSingleChild(depositElement, Constants.DEPOSITOR).
                             getTextContent().trim());

        deposit.setAmount(getSingleChild(depositElement, Constants.AMOUNT).
                          getTextContent().trim());

        deposit.setProfitability(getSingleChild(depositElement, Constants.PROFITABILITY).
                                 getTextContent().trim());

        deposit.setTimeConstraints(getSingleChild(depositElement, Constants.TIME_CONSTRAINTS).
                                   getTextContent().trim());

        deposit.setStartDateOfDeposit(getSingleChild(depositElement, Constants.START_DATE_OF_DEPOSIT).
                                      getTextContent().trim());

    }

    private  Element getSingleChild(Element element, String childName){
        NodeList nlist = element.getElementsByTagName(childName);
        Element child = (Element) nlist.item(0);
        return child;
    }
}
