package server.by.tc.task2.server.service;



public final class ParserType {
    public static final String SAX = "SAX";
    public static final String STAX = "STAX";
    public static final String DOM = "DOM";
}