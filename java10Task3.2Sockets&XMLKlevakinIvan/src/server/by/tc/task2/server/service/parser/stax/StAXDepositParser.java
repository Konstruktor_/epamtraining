package server.by.tc.task2.server.service.parser.stax;


import server.by.tc.task2.server.entity.Deposit;
import server.by.tc.task2.server.entity.DepositTagName;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;



public class StAXDepositParser {

    private ArrayList<Deposit> depositList = new ArrayList<Deposit>();
    private Deposit deposit;
    private DepositTagName elementName;

    private XMLInputFactory inputFactory;
    private InputStream input;
    private XMLStreamReader streamReader;


    public ArrayList<Deposit> parse(String path) throws XMLStreamException, FileNotFoundException {

        inputFactory = XMLInputFactory.newInstance();
        input = new FileInputStream(path);
        streamReader = inputFactory.createXMLStreamReader(input);
        handle(streamReader);

        return depositList;
    }

    private void handle(XMLStreamReader reader) throws XMLStreamException {

        while (reader.hasNext()) {

            int type = reader.next();
            switch (type) {

                case XMLStreamConstants.START_ELEMENT:
                    findStartElement(reader);
                    break;

                case XMLStreamConstants.CHARACTERS:
                    String text = reader.getText().trim();

                    if (text.isEmpty()) {
                        break;
                    }

                    findElements(text);
                    break;

                case XMLStreamConstants.END_ELEMENT:

                    findEndElement(reader);
            }
        }
    }

    private void findStartElement(XMLStreamReader reader){
        elementName = DepositTagName.getElementTagName(reader.getLocalName());
        switch (elementName) {
            case DEPOSIT:
                deposit = new Deposit();
                Integer id = Integer.parseInt(reader.getAttributeValue(null, "id"));
                deposit.setId(id);
                break;
        }
    }

    private void findElements(String text){

        switch (elementName) {
            case CURRENCY:
                deposit.setCurrency(text.toString());
                break;
            case TYPE:
                deposit.setType(text.toString());
                break;
            case DEPOSITOR:
                deposit.setDepositor(text.toString());
                break;
            case AMOUNT:
                deposit.setAmount(text.toString());
                break;
            case PROFITABILITY:
                deposit.setProfitability(text.toString());
                break;
            case TIME_CONSTRAINTS:
                deposit.setTimeConstraints(text.toString());
                break;
            case START_DATE_OF_DEPOSIT:
                deposit.setStartDateOfDeposit(text.toString());
                break;
        }
    }

    private void findEndElement(XMLStreamReader reader){
        elementName = DepositTagName.getElementTagName(reader.getLocalName());
        switch (elementName) {

            case DEPOSIT:
                depositList.add(deposit);
        }
    }

}
