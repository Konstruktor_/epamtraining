package server.by.tc.task2.server.entity;

public class Constants {
    public static final String DEPOSIT = "deposit";
    public static final String CURRENCY = "currency";
    public static final String TYPE = "type";
    public static final String DEPOSITOR = "depositor";
    public static final String AMOUNT = "amount";
    public static final String PROFITABILITY = "profitability";
    public static final String TIME_CONSTRAINTS = "time-constraints";
    public static final String START_DATE_OF_DEPOSIT = "start-date-of-deposit";
    public static final String BANK_DEPOSITS = "bank-deposits";
    public static final String ID = "id";
}
