package server.by.tc.task2.server.entity;


public enum DepositTagName {
    CURRENCY, TYPE, DEPOSITOR, AMOUNT, PROFITABILITY, TIME_CONSTRAINTS,
    START_DATE_OF_DEPOSIT, DEPOSIT, BANK_DEPOSITS;


    public static DepositTagName getElementTagName(String element) {
        switch (element) {
            case Constants.DEPOSIT:
                return DEPOSIT;
            case Constants.CURRENCY:
                return CURRENCY;
            case Constants.TYPE:
                return TYPE;
            case Constants.DEPOSITOR:
                return DEPOSITOR;
            case Constants.AMOUNT:
                return AMOUNT;
            case Constants.PROFITABILITY:
                return PROFITABILITY;
            case Constants.TIME_CONSTRAINTS:
                return TIME_CONSTRAINTS;
            case Constants.START_DATE_OF_DEPOSIT:
                return START_DATE_OF_DEPOSIT;
            case Constants.BANK_DEPOSITS:
                return BANK_DEPOSITS;
            default:
                throw new EnumConstantNotPresentException(DepositTagName.class, element);
        }
    }
}
