package client.by.epam.task2.client.controller;


public final class ParserType {
    public static final String SAX = "SAX";
    public static final String STAX = "STAX";
    public static final String DOM = "DOM";
}