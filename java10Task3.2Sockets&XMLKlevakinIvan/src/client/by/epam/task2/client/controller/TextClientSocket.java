package client.by.epam.task2.client.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import client.by.epam.task2.client.entity.Deposit;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Scanner;

public class TextClientSocket {

	private static final Logger log = LogManager.getLogger(TextClientSocket.class);

	private String ip;
	private int port;

	private Socket socket;
	private InputStream in;
	private OutputStream out;

	public TextClientSocket(String ip, int port) {
		this.ip = ip;
		this.port = port;
	}

	public void start() throws ClassNotFoundException {
		try {
			socket = new Socket(ip, port);

			log.info("Client connected to server.");

			in = socket.getInputStream();
			out = socket.getOutputStream();

			byte[] request = createRequest();
			ArrayList<Deposit> response;

			while (request != null) {

				sendRequestToServer(request);

				response = readAnswerFromServer();

				showResponse(response);

				request = createRequest();
			}
		} catch (IOException | InterruptedException e) {
			log.error(e);
		} finally {
			try {
				if (sendCloseRequestToServer()) {
					socket.close();
					log.info("Connection with server successfully closed.");
				} else {
					socket.close();
					log.info("Connection with server closed. Server didn't answer.");
				}
			} catch (IOException | InterruptedException e) {
				log.error(e);
			}
		}
	}

	private byte[] createRequest() throws InterruptedException, IOException {
		log.info("Create request");
		Scanner sc = new Scanner(System.in);
		System.out.print("\nsax  - parsing with SAXParser \n" +
				         "stax - parsing with StAXParser \n" +
				         "dom  - parsing with DOMParser \n" +
				         "other symbols - close connect with server \nEnter:");
		String i = sc.nextLine().toUpperCase();
		switch (i) {
		case ParserType.SAX:
			return createTextStream(ParserType.SAX);
		case ParserType.STAX:
			return createTextStream(ParserType.STAX);
		case ParserType.DOM:
			return createTextStream(ParserType.DOM);
		default:
			return null;
		}
	}
	
	private ArrayList<Deposit> readAnswerFromServer() throws InterruptedException, IOException, ClassNotFoundException {
		log.info("Read answer from server.");
		ArrayList<Deposit> depositList;

		ObjectInputStream ois = new ObjectInputStream(in);

		depositList =(ArrayList<Deposit>) ois.readObject();
		return depositList;
	}

	private void sendRequestToServer(byte[] b) throws IOException {
		log.info("Send request.");
		out.write(b);
	}

	private void showResponse(ArrayList<Deposit> deposits) {
		log.info("Show response.");
		System.out.println(deposits.toString());
	}

	private byte[] createTextStream(String text) throws IOException {
		log.info("Create text stream.");
		ByteArrayOutputStream out = new ByteArrayOutputStream();

		out.write(text.getBytes());

		byte[] request = out.toByteArray();

		out.close();

		return request;
	}

	private boolean sendCloseRequestToServer() throws IOException, InterruptedException {
		log.info("Send close request to server...");
		out.write(new byte[] { 0 });

		byte[] response = new byte[1];

		in.read(response);

		if (response[0] == 0) {
			return true;
		}
		return false;
	}
}
