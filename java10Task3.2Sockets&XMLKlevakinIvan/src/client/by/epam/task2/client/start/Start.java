package client.by.epam.task2.client.start;


import client.by.epam.task2.client.controller.TextClientSocket;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.core.LoggerContext;

import java.io.File;

public class Start {

	public static void main(String[] args) throws ClassNotFoundException {

		LoggerContext context = (LoggerContext) LogManager.getContext(false);
		File file = new File(System.getProperty("user.dir")+ "\\src\\client\\log4j2.xml");
		context.setConfigLocation(file.toURI());

		TextClientSocket client = new TextClientSocket("localhost", 4830);
		client.start();

	}

}
