package client.by.epam.task2.client.entity;

import java.io.Serializable;


public class Deposit implements Serializable {

    private static final long serialVersionUID = 5950169519310163575L;

    private int id;

    private String currency;
    private String type;
    private String depositor;
    private String amount;
    private String profitability;
    private String timeConstraints;
    private String startDateOfDeposit;

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDepositor(String depositor) {
        this.depositor = depositor;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setProfitability(String profitability) {
        this.profitability = profitability;
    }

    public void setTimeConstraints(String timeConstraints) {
        this.timeConstraints = timeConstraints;
    }

    public void setStartDateOfDeposit(String startDateOfDeposit) {
        this.startDateOfDeposit = startDateOfDeposit;
    }

    public String getCurrency() {
        return currency;
    }

    public String getType() {
        return type;
    }

    public String getDepositor() {
        return depositor;
    }

    public int getId() {
        return id;
    }

    public String getAmount() {
        return amount;
    }

    public String getProfitability() {
        return profitability;
    }

    public String getTimeConstraints() {
        return timeConstraints;
    }

    public String getStartDateOfDeposit() {
        return startDateOfDeposit;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Deposit deposit = (Deposit) o;

        if (id != deposit.id) {
            return false;
        }
        if (currency != null ? !currency.equals(deposit.currency)
                             : deposit.currency != null) {
            return false;
        }
        if (type != null ? !type.equals(deposit.type)
                         : deposit.type != null) {
            return false;
        }
        if (depositor != null ? !depositor.equals(deposit.depositor)
                              : deposit.depositor != null) {
            return false;
        }
        if (amount != null ? !amount.equals(deposit.amount) : deposit.amount != null) {
            return false;
        }
        if (profitability != null ? !profitability.equals(deposit.profitability)
                                  : deposit.profitability != null) {
            return false;
        }
        if (timeConstraints != null ? !timeConstraints.equals(deposit.timeConstraints)
                                    : deposit.timeConstraints != null) {
            return false;
        }
        return !(startDateOfDeposit != null ? !startDateOfDeposit.equals(deposit.startDateOfDeposit)
                                            : deposit.startDateOfDeposit != null);

    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + currency.hashCode() + result + type.hashCode()
                 + depositor.hashCode() + amount.hashCode() + profitability.hashCode()
                 + timeConstraints.hashCode() + startDateOfDeposit.hashCode();
        return result;
    }

    @Override
    public String toString() {
        String string = getClass().getName() + "@id: " + id + "\n" +
                        "currency: " + currency + "\n" +
                        "type: " + type + "\n" +
                        "depositor: " + depositor + "\n" +
                        "amount: " + amount + "\n" +
                        "profitability: " + profitability + "\n" +
                        "timeConstraints: " + timeConstraints + "\n" +
                        "startDateOfDeposit: " + startDateOfDeposit+ "\n";
        return string;
    }
}
