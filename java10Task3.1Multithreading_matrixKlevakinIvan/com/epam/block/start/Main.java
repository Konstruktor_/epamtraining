package com.epam.block.start;

import com.epam.block.entity.Array;
import com.epam.block.service.Marker;



public class Main {
    public static void main(String[] args) {

        int n = 10000;
        Array arr = new Array(n);

        Marker marker1 = new Marker(arr, 1);
        Marker marker2 = new Marker(arr, 2);

        marker1.start();
        marker2.start();

        while(arr.nextEmptyDiagonalCell() != -1) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < n ; i++) {
            System.out.println(i + ": " + arr.getDiagonalElem(i));
        }
    }
}
