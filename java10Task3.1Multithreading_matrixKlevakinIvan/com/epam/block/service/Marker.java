package com.epam.block.service;

import com.epam.block.entity.Array;



public class Marker extends Thread {

    private Array arr;
    private int markerSign;

    public Marker(Array arr, int markerSign) {
        this.arr = arr;
        this.markerSign = markerSign;
    }


    @Override
    public void run() {

        int index = arr.nextEmptyDiagonalCell();

    do {
    synchronized (arr) {
        if (index == -1){
            break;
        }
        index = arr.markAndCount(markerSign);
    }
    } while(true);

    }
}