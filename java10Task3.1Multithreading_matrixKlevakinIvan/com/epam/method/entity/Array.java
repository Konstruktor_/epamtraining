package com.epam.method.entity;

import java.util.Arrays;



public class Array {
    private int n;
    private int[][] arr;
    private int count = 0;

    public Array(int n) {
        this.n = n;
        this.arr = new int[n][n];
    }

    public synchronized int markAndCount(int markerSign) {

        int index = nextEmptyDiagonalCell();

        if (index != -1) {
            setDiagonalElem(index, markerSign);
            if (++count < n) {
                return count;
            }
        }
        return -1;
    }

    public int nextEmptyDiagonalCell() {
        return count == n ? -1 : count;
    }

    public void setDiagonalElem(int i, int value) {
        this.arr[i][i] = value;
    }

    public int getDiagonalElem(int i) {
        return arr[i][i];
    }



    @Override
    public String toString() {
        return "Array{" +
                "n=" + n +
                ", arr=" + Arrays.deepToString(arr) +
                '}';
    }
}
