package com.epam.method.start;

import com.epam.method.entity.Array;
import com.epam.method.service.Marker;


public class Main {
    public static void main(String[] args) {

        int n = 10000;
        Array arr = new Array(n);

        Marker marker1 = new Marker(arr, 1);
        Marker marker2 = new Marker(arr, 2);

        marker2.start();
        marker1.start();

        while(arr.nextEmptyDiagonalCell() != -1) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        for (int i = 0; i < n ; i++) {
            System.out.println(i + ": " + arr.getDiagonalElem(i));
        }
    }
}
