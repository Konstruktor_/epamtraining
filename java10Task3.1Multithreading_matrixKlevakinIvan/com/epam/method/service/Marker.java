package com.epam.method.service;

import com.epam.method.entity.Array;



public class Marker extends Thread {

    private Array arr;
    private int markerSign;

    public Marker(Array arr, int markerSign) {
        this.arr = arr;
        this.markerSign = markerSign;
    }


    @Override
    public void run() {

        int index = arr.nextEmptyDiagonalCell();
        while(index != -1) {

            index = arr.markAndCount(markerSign);
        }
    }
}
