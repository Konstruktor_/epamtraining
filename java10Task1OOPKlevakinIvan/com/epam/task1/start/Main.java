package com.epam.task1.start;

import com.epam.task1.service.DOMParser;
import com.epam.task1.service.XMLReader;

import java.io.IOException;


public class Main {
    public static void main(String[] args) throws IOException {

        String name = System.getProperty("user.dir") + "\\src\\resources\\menu.xml";

        XMLReader reader = new XMLReader(name);
        DOMParser parser = new DOMParser();

        parser.parse(reader);

        reader.close();

        System.out.println(parser.getDocument().toString());

        System.out.println(parser.getDocument().getRoot().getElement(0).toString());
        System.out.println(parser.getDocument().getRoot().getElement(1).toString());
        System.out.println(parser.getDocument().getRoot().getElement(2).toString());
        System.out.println(parser.getDocument().getRoot().getElement(3).toString());
        System.out.println(parser.getDocument().getRoot().getElement(4).toString());


    }
}
