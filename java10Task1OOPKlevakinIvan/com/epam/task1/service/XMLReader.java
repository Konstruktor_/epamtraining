package com.epam.task1.service;

import java.io.*;


public class XMLReader extends BufferedReader implements Reader {

    public XMLReader(java.io.Reader in) {
        super(in);
    }

    public XMLReader(String path) throws FileNotFoundException {
        super(new XMLReader(new FileReader(new File(path).getAbsoluteFile())));
    }

    @Override
    public void close() throws IOException{
        super.close();
    }

    @Override
    public String readLine() throws IOException {

        String line = super.readLine();
        String result = "";

        while (line != null){
            if (!line.endsWith(">")) {
                result += line;
            } else {
                result += line;
                break;
            }
            line = super.readLine();
        }

        if(result.isEmpty()) {
            return null;
        } else {
            return result.replaceAll("[\\s]{2,}", " ");
        }
    }
}
