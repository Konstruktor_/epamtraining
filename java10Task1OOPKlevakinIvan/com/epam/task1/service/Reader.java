package com.epam.task1.service;


import java.io.Closeable;
import java.io.IOException;

public interface Reader extends Closeable {

    void close() throws IOException;

    String readLine() throws IOException;

}
