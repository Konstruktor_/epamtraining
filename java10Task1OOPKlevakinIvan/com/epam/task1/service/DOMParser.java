package com.epam.task1.service;

import com.epam.task1.entity.Attribute;
import com.epam.task1.entity.Document;
import com.epam.task1.entity.Element;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DOMParser {

    private Stack<String> stack = new Stack<>();
    private ArrayList<Element> list = new ArrayList<>();
    private Document document = new Document();
    private Element elementVariable;
    private int level = -1;

    private static final String REGULAR_START_TAG = "(<(?<tagWithoutAttribute>[^<>/=]+)>)" +
                                            "|(<(?<tagWithAttribute>[\\w]+)[^<>]+>)";

    private static final String REGULAR_END_TAG = "</(?<endTag>[^\\s<>//]+)>";

    private static final String REGULAR_CONTENT = "[>\\s]{1}(?<content>[^<>]+)<";

    private static final String REGULAR_ATTRIBUTE = "(?<attributeName>[a-zA-z]+)={1}['|\\\"]{1}" +
                                            "(?<attributeValue>([-\\w]+))['|\\\"]{1}";

    private static final String TAG_WITHOUT_ATTRIBUTE = "tagWithoutAttribute";
    private static final String TAG_WITH_ATTRIBUTE = "tagWithAttribute";
    private static final String END_TAG = "endTag";
    private static final String CONTENT = "content";
    private static final String ATTRIBUTE_NAME ="attributeName";
    private static final String ATTRIBUTE_VALUE ="attributeValue";


    private static final Pattern PATTERN_START_TAG =  Pattern.compile(REGULAR_START_TAG);
    private static final Pattern PATTERN_END_TAG = Pattern.compile(REGULAR_END_TAG);
    private static final Pattern PATTERN_CONTENT = Pattern.compile(REGULAR_CONTENT);
    private static final Pattern PATTERN_ATTRIBUTE = Pattern.compile(REGULAR_ATTRIBUTE);


    public void parse(XMLReader XMLReader) throws IOException {

        String line = XMLReader.readLine();

        while (line != null) {

            Matcher matcherStartTag = PATTERN_START_TAG.matcher(line);
            Matcher matcherEndTag = PATTERN_END_TAG.matcher(line);
            Matcher matcherContent = PATTERN_CONTENT.matcher(line);
            Matcher matcherAttribute = PATTERN_ATTRIBUTE.matcher(line);

            while (matcherStartTag.find()) {                                          // find all start tags(elements) on line

                elementVariable = new Element();

                if (level < 0) {                                                      // find root tag(element)

                    handlingRootTag(matcherStartTag, matcherAttribute);

                } else if (matcherStartTag.group(TAG_WITHOUT_ATTRIBUTE) != null) {    // find start tags(elements)...
                                                                                      //  without attributes after root
                    handlingStartTag(matcherStartTag,TAG_WITHOUT_ATTRIBUTE);

                } else if (matcherStartTag.group(TAG_WITH_ATTRIBUTE) != null) {       // find start tags(elements)...
                                                                                      //  with attributes after root
                    handlingStartTag(matcherStartTag,TAG_WITH_ATTRIBUTE);

                    findAttributes(matcherAttribute);
                }
            }

            findContent(matcherContent);                                              //find content between tags
            findEndTags(matcherEndTag);                                               // find end of tags(elements)

            line = XMLReader.readLine();
        }

        XMLReader.close();
    }


    private void handlingRootTag(Matcher matcherStartTag, Matcher matcherAttribute){

        document.setRoot(elementVariable);
        list.add(elementVariable);
        level++;

        if (matcherStartTag.group(TAG_WITHOUT_ATTRIBUTE) != null) {                 // root element without attribute

            stack.push(matcherStartTag.group(TAG_WITHOUT_ATTRIBUTE));
            elementVariable.setName(matcherStartTag.group(TAG_WITHOUT_ATTRIBUTE));

        } else {                                                                    // root element with attribute

            stack.push(matcherStartTag.group(TAG_WITH_ATTRIBUTE));
            elementVariable.setName(matcherStartTag.group(TAG_WITH_ATTRIBUTE));

            findAttributes(matcherAttribute);
        }
    }


    private void handlingStartTag(Matcher matcherStartTag,String typeOfTag) {

        level++;
        elementVariable.setName(matcherStartTag.group(typeOfTag));
        stack.push(matcherStartTag.group(typeOfTag));
        list.add(elementVariable);
        list.get(level - 1).addElement(list.get(level));
    }

    private void findEndTags(Matcher matcherEndTag){

        while (matcherEndTag.find()){
            if (stack.peek().equals(matcherEndTag.group(END_TAG))){
                stack.pop();
                list.remove(level);
                level--;
            }
        }
    }

    private void findAttributes(Matcher matcherAttribute){

        while (matcherAttribute.find()) {
            Attribute attr = new Attribute();
            attr.setName(matcherAttribute.group(ATTRIBUTE_NAME));
            attr.setValue(matcherAttribute.group(ATTRIBUTE_VALUE));
            elementVariable.addAttribute(attr);
        }
    }


    private void findContent(Matcher matcherContent) {                              //find content between tags
        if (matcherContent.find()) {
            list.get(level).setContent(matcherContent.group(CONTENT));
        }
    }

    public Document getDocument(){
        return document;
    }

}
