package com.epam.task1.entity;

import java.util.ArrayList;
import java.util.List;



public class Element {

    private String name;
    private String content;
    private ArrayList<Element> elements = new ArrayList<Element>();
    private ArrayList<Attribute> attributes = new ArrayList<Attribute>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Element getElement(int i) {
        return elements.get(i);
    }

    public void addElement(Element element) {
        elements.add(element);
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<Element> getElements() {
        return elements;
    }

    public List<Attribute> getAttributes() {
        return attributes;
    }

    public void addAttribute(Attribute attribute) {
        attributes.add(attribute);
    }

    public Attribute getAttribute(int i) {
        return attributes.get(i);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Element element = (Element) obj;

        if (name == null) {
            if (element.getName() != null) {
                return false;
            }
        } else if (!name.equals(element.name)){
            return  false;
        }

        if (content == null) {
            if (element.content != null) {
                return false;
            }
        } else if (!content.equals(element.name)){
            return false;
        }

        if (elements == null) {
            if (element.elements != null){
                return false;
            }
        } else if (!elements.equals(element.elements)){
            return false;
        }

        if (attributes == null) {
            if (element.attributes != null){
                return false;
            }
        } else if (!attributes.equals(element.attributes)){
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (content == null) ? 0 : content.hashCode();
        result += (name == null) ? 0 : name.hashCode();
        result += (elements == null) ? 0 : elements.hashCode();
        result += (attributes == null)? 0 : attributes.hashCode();
        return result;
    }

    @Override
    public String toString() {
        String string = getClass().getName() + "@name: " + name +
        ", content: " + content + "\n";

        for (Attribute attribute : attributes) {
            string += attribute.toString();
        }

        for (Element element : elements) {
            string += element.toString();
        }

        return string;
    }
}
