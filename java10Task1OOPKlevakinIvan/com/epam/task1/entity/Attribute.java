package com.epam.task1.entity;


public class Attribute {

    private String name;
    private String value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Attribute attribute = (Attribute) obj;

        if (name == null) {
            if (attribute.name != null){
                return false;
            }
        }else if (!name.equals(attribute.name)) {
            return false;
        }

        if (value == null) {
            if (attribute.value != null){
                return false;
            }
        }else if (!value.equals(attribute.value)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (name == null) ? 0 : name.hashCode();
        result += (value == null) ? 0 : value.hashCode();
        return result;
    }

    @Override
    public String toString() {
        String string = getClass().getName() + "@name: " + name + ", value: " + value +"\n";
        return string;
    }
}
