package com.epam.task1.entity;



public class Document {

    private Element root;

    public Element getRoot() {
        return root;
    }

    public void setRoot(Element root) {
        this.root = root;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj){
            return true;
        }

        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }

        Document document = (Document) obj;

        if (root == null) {
            if (document.root != null){
                return false;
            }
        } else if (!root.equals(document.root)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (root != null) ? root.hashCode() : 0;
        return result;
    }

    @Override
    public String toString() {
        String string = getClass().getName() + "@name: " + root.getName() + "\n";
        string += root.toString();
        return string;
    }
}