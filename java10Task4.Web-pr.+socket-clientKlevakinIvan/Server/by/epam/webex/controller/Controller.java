package by.epam.webex.controller;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class Controller extends HttpServlet {
    private static final long serialVersionUID = 1L;


    public Controller() {
        super();
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        // TODO Auto-generated method stub
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        String param1Name = request.getParameter("param1");
        String param2Name = request.getParameter("param2");

        response.setContentType("text/html; charset=utf-8");

        PrintWriter out = response.getWriter();
        out.print("<h3>Hello! I get POST.Request with param1 = "
                  + param1Name + ", param2 = " + param2Name);
        out.close();


    }

}