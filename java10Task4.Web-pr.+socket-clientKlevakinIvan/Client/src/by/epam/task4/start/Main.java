package by.epam.task4.start;

import by.epam.task4.logic.HTTPResponseParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;


import java.io.*;
import java.net.Socket;

public class Main {

    private static final Logger log =  LogManager.getLogger(Main.class);

    public static void main(String[] args) throws IOException, InterruptedException {

        LoggerContext context = (LoggerContext) LogManager.getContext(false);
        File file = new File(System.getProperty("user.dir")+ "\\src\\log4j2.xml");
        context.setConfigLocation(file.toURI());

        Socket socket = new Socket("127.0.0.1", 8080);

        System.out.println("Клиент: соединение установлено.");

        BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        OutputStream out = socket.getOutputStream();



        String request = "POST http://localhost:8080/Server/index.jsp HTTP/1.1\n"+
                "Accept-Language: ru\n"+
                "Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, */*\n"+
                "Proxy-Connection: Keep-Alive\n"+
                "User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.0; .NET CLR 1.0.3705)\n"+
                "Host: localhost\n"+ "\n" +
                "param1=1&param2=2;";

        out.write(request.getBytes());

        HTTPResponseParser parser = new HTTPResponseParser(in);
        parser.parseHTTPResponse();
        System.out.println(parser.getHttpResponse().toString());

        in.close();
        out.close();
        socket.close();

        System.out.println("Завершено.");
    }
}
