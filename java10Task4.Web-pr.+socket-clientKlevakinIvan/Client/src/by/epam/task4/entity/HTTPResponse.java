package by.epam.task4.entity;

import java.util.ArrayList;

public class HTTPResponse {

    private ResponseStatusLine responseStatusLine;
    private ArrayList<ResponseHeaderField> listOfFields = new ArrayList<ResponseHeaderField>();
    private ResponseBody responseBody = new ResponseBody();


    public void addResponseHeaderField(ResponseHeaderField headerField){
        listOfFields.add(headerField);
    }

    public void setResponseStatusLine(ResponseStatusLine responseStatusLine) {
        this.responseStatusLine = responseStatusLine;
    }

    public void setResponseBody(ResponseBody responseBody){
        this.responseBody = responseBody;
    }


    @Override
    public String toString() {

        String string = getClass().getName() + "@responseStatusLine:" + "\n"
                       + responseStatusLine.toString();
        string += "listOfFields:" + "\n" +listOfFields.toString();
        string += "\nresponseBody:\n[" + responseBody.toString() + "]";
        return string;
    }
}
