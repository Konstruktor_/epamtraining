package by.epam.task4.entity;

public class ResponseStatusLine {

    private String httpVersion;
    private String statusCode;
    private String reasonPhrase;

    public ResponseStatusLine(String httpVersion, String statusCode, String reasonPhrase) {
        this.httpVersion = httpVersion;
        this.statusCode = statusCode;
        this.reasonPhrase = reasonPhrase;
    }

    @Override
    public String toString() {

        String string = "[" + getClass().getName() + "@ Версия протокола: " + httpVersion +
                        ". Код состояния: " + statusCode + ". Поясняющая фраза: " + reasonPhrase + "]\n";
        return string;
    }


}
