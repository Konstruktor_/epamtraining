package by.epam.task4.entity;

/**
 * Created by Ivan on 28.01.2016.
 */
public class ResponseBody {

    private StringBuilder responseBody = new StringBuilder();

    public void addLineToResponseBody(String responseBody) {
        this.responseBody.append(responseBody);
    }

    @Override
    public String toString() {
        String string = getClass().getName() + "@responseBody:\n" + responseBody;
        return string;
    }
}
