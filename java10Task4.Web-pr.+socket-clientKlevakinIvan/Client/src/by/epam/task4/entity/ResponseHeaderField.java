package by.epam.task4.entity;

public class ResponseHeaderField {

    private String fieldName;
    private String fieldValue;

    public ResponseHeaderField(String fieldName, String fieldValue) {
        this.fieldName = fieldName;
        this.fieldValue = fieldValue;
    }


    @Override
    public String toString() {

        String string = getClass().getName() +"@fieldName: "+ fieldName +
                        fieldValue + "\n";
        return string;
    }
}
