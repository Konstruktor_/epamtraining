package by.epam.task4.entity;

public final class HeaderFieldName {
    public static final String CACHE_CONTROL = "cache-control";
    public static final String DATE = "date";
    public static final String SERVER = "server";
    public static final String SET_COOKIE = "set-cookie";
    public static final String CONTENT_TYPE = "content-type";
    public static final String CONTENT_LENGTH = "content-length";
}
