package by.epam.task4.logic;

import by.epam.task4.entity.*;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;

public class HTTPResponseParser {

    private static final Logger log = LogManager.getLogger(HTTPResponseParser.class);

    private String fieldNameRU;
    private String fieldName;
    private String fieldValue;
    private String responseLine;

    private ResponseHeaderField responseHeaderField;
    private ResponseStatusLine responseStatusLine;
    private ResponseBody responseBody = new ResponseBody();

    private HTTPResponse httpResponse = new HTTPResponse();

    private BufferedReader in;


    public HTTPResponseParser(BufferedReader in) {
        this.in = in;
    }

    public void parseHTTPResponse() throws IOException {

        parseStatusLine();

        responseLine = in.readLine();

        while(!responseLine.isEmpty()){

            responseHeaderField = parseField(responseLine);
            httpResponse.addResponseHeaderField(responseHeaderField);
            responseLine = in.readLine();
        }

        parseBody();

    }

    private void parseStatusLine(){

        try {
            responseLine = in.readLine();
        } catch (IOException e) {
            log.error("Ops!", e);
        }

        String[] partsOfStatusLine = responseLine.split(" ");

        String httpVersion = partsOfStatusLine[0];
        String statusCode = partsOfStatusLine[1];
        String reasonPhrase = partsOfStatusLine[2];

        responseStatusLine= new ResponseStatusLine(httpVersion, statusCode, reasonPhrase);

        httpResponse.setResponseStatusLine(responseStatusLine);

    }

    private ResponseHeaderField parseField(String s){

        String[] partsOfFieldLine = s.split(": ");

        fieldName = partsOfFieldLine[0];
        fieldValue = partsOfFieldLine[1];

        switch (fieldName.toLowerCase()) {

            case HeaderFieldName.CACHE_CONTROL:
                fieldNameRU = "Кэш-контроль(Cache-Control):";
                responseHeaderField = new ResponseHeaderField(fieldNameRU, fieldValue);
                return responseHeaderField;

            case HeaderFieldName.SERVER:
                fieldNameRU = "Сервер(Server):";
                responseHeaderField = new ResponseHeaderField(fieldNameRU, fieldValue);
                return responseHeaderField;

            case HeaderFieldName.CONTENT_LENGTH:
                fieldNameRU = "Длина документа(Content-Length):";
                responseHeaderField = new ResponseHeaderField(fieldNameRU, fieldValue);
                return responseHeaderField;

            case HeaderFieldName.DATE:
                fieldNameRU = "Дата(Date):";
                responseHeaderField = new ResponseHeaderField(fieldNameRU, fieldValue);
                return responseHeaderField;

            case HeaderFieldName.CONTENT_TYPE:
                fieldNameRU = "Тип документа(Content-Type):";
                responseHeaderField = new ResponseHeaderField(fieldNameRU, fieldValue);
                return responseHeaderField;

            case HeaderFieldName.SET_COOKIE:
                fieldNameRU = "Куки(Set-Cookie):";
                responseHeaderField = new ResponseHeaderField(fieldNameRU, fieldValue);
                return responseHeaderField;

            default:
                return null;
        }
    }

    private void parseBody(){
        try {
            String sp = "\n";
            responseLine = in.readLine();
            while (responseLine != null){
                if (!responseLine.isEmpty()){
                    responseBody.addLineToResponseBody(responseLine + sp);
                }
                responseLine = in.readLine();

            }
            in.close();

            httpResponse.setResponseBody(responseBody);

        } catch (IOException e) {
            log.error("Ops!", e);
        }

    }

    public HTTPResponse getHttpResponse() {
        return httpResponse;
    }
}
